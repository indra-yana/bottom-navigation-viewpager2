package com.indrayana.bottomnavigationviewpager2.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.indrayana.bottomnavigationviewpager2.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class LiveTVFragment extends Fragment {


    public LiveTVFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_live_tv, container, false);
    }

}
