package com.indrayana.bottomnavigationviewpager2.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.indrayana.bottomnavigationviewpager2.R;
import com.indrayana.bottomnavigationviewpager2.adapter.HomeViewPager2Adapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private static String TAB_POSITION_KEY = "TAB_POSITION_KEY";

    private ViewPager2 mViewPager;
    private HomeViewPager2Adapter viewPagerAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mViewPager = view.findViewById(R.id.view_pager);
        mViewPager.setOffscreenPageLimit(3);

        setupViewPager(mViewPager);

        final TabLayout tabLayout = view.findViewById(R.id.tab_layout);

        // Implementing tab selected listener over tablayout
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //setting current selected item over viewpager
                mViewPager.setCurrentItem(tab.getPosition());

//                Bundle bundle = new Bundle();
//                bundle.putInt(TAB_POSITION_KEY, tab.getPosition());
//                setArguments(bundle);
//
//                if (getArguments() != null) {
//                    mViewPager.setCurrentItem(getArguments().getInt(TAB_POSITION_KEY));
//                } else {
//                    mViewPager.setCurrentItem(tab.getPosition());
//                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        new TabLayoutMediator(tabLayout, mViewPager, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText(viewPagerAdapter.getPageTitle(position));
            }
        }).attach();
    }

    private void setupViewPager(ViewPager2 viewPager) {
        viewPagerAdapter = new HomeViewPager2Adapter(this);
        for (int i=0; i<10; i++) {
            viewPagerAdapter.addFragment(HomeFragmentContent.newInstance(Integer.toString(i)), "Fragment " +i);
        }
        viewPager.setAdapter(viewPagerAdapter);
    }
}
