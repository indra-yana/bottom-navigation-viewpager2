package com.indrayana.bottomnavigationviewpager2;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.indrayana.bottomnavigationviewpager2.adapter.ViewPager2Adapter;
import com.indrayana.bottomnavigationviewpager2.fragment.HomeFragment;
import com.indrayana.bottomnavigationviewpager2.fragment.LiveTVFragment;
import com.indrayana.bottomnavigationviewpager2.fragment.MoviesFragment;
import com.indrayana.bottomnavigationviewpager2.fragment.SettingsFragment;

public class MainActivity extends AppCompatActivity {

    private ViewPager2 viewPager2;
    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        viewPager2.setCurrentItem(0);
                        return true;
                    case R.id.live_tv_menu:
                        viewPager2.setCurrentItem(1);
                        return true;
                    case R.id.movies_menu:
                        viewPager2.setCurrentItem(2);
                        return true;
                    case R.id.settings_menu:
                        viewPager2.setCurrentItem(3);
                        return true;
                }
                return false;
            }
        });

        viewPager2 = findViewById(R.id.viewpager);
        viewPager2.setUserInputEnabled(false);

        setupViewPager(viewPager2);

        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                setSelectedBottomNavigation(position);
            }
        });
    }

    private void setupViewPager(ViewPager2 viewPager) {
        ViewPager2Adapter viewPager2Adapter = new ViewPager2Adapter(this);

        viewPager2Adapter.addFragment(new HomeFragment(), getString(R.string.home));
        viewPager2Adapter.addFragment(new LiveTVFragment(), getString(R.string.live_tv));
        viewPager2Adapter.addFragment(new MoviesFragment(), getString(R.string.movies));
        viewPager2Adapter.addFragment(new SettingsFragment(), getString(R.string.settings));

        viewPager.setAdapter(viewPager2Adapter);
    }

    @Override
    public void onBackPressed() {
        if (viewPager2.getCurrentItem() > 0) {
            //if any tab selected instead of tab 1
            int position = viewPager2.getCurrentItem() - 1;

            viewPager2.setCurrentItem(position);
            setSelectedBottomNavigation(position);
        } else if (viewPager2.getCurrentItem() == 0) {
            //if tab 0 selected
            super.onBackPressed();
        }
    }

    private void setSelectedBottomNavigation(int position) {
        switch (position) {
            case 0:
                bottomNavigationView.setSelectedItemId(R.id.home_menu);
                break;
            case 1:
                bottomNavigationView.setSelectedItemId(R.id.live_tv_menu);
                break;
            case 2:
                bottomNavigationView.setSelectedItemId(R.id.movies_menu);
                break;
            case 3:
                bottomNavigationView.setSelectedItemId(R.id.settings_menu);
                break;
        }
    }
}
